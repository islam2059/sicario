# Sicario #

Sicario is a gradient boosting classifier for the reliable detection of true genomic variants, particularly indels. It demonstrated excellent performance when trained with the gold-standard variant dataset from “Genome in a Bottle” (GIAB) consortium.

## Installing Prerequisites ##

Sicario has been tested on Ubuntu 20.04 LTS. However, earlier Ubuntu as well as other Debian-based Linux distributions should also work fine.
We need to install tabix. Sicario calls this internally to index VCF files.
>**sudo apt install tabix**

We install Python3. Our example requires some files to be fetched from Google cloud bucket. Using Python3 wrapper for gsutil is an easy way to do this.
>**sudo apt-get install python3  
sudo apt install python3-pip  
sudo pip3 install -U gsutil**

We need to install megatools, since the annotation files used by Sicario to annotate VCF are hosted in our MEGA file hosting service. Using megatools we can access and download the files using command line interface.
>**sudo apt-get install megatools**

Finally to run sicario.jar file we need to install Java SE Runtime environment

>**sudo apt install default-jre**

## Preparing Directory Structure ##

Sicario uses several types of annotation and variant files and produces multiple output files. To organize them we suggest to create following directory structure.

>**cd $HOME  
mkdir Test  
mkdir Test/cdata  
mkdir Test/cdata/reference  
mkdir Test/cdata/reference/hg19  
mkdir Test/cdata/giab  
mkdir Test/cdata/giab/vcf  
mkdir Test/cdata/giab/bed  
mkdir Test/cdata/gerp  
mkdir Test/cdata/annotation  
mkdir Test/sicario  
mkdir Test/sicario/vcf  
mkdir Test/sicario/bed  
mkdir Test/sicario/evl  
mkdir Test/sicario/mod  
mkdir Test/sicario/prd  
mkdir Test/sicario/svm**

## Downloading Annotation Files ##

We use megadl to downlaod the RepeatMasker annotation file.
>**cd ${HOME}/Test/cdata/annotation  
megadl 'https://mega.nz/#!iRIyGQSY!5xfwMZX9H2Lk1_JirWkL_zweaRmGt5UUy7wlZDksaWs'**

We download GERP score files for chromosome 1 to 22
>**cd ${HOME}/Test/cdata/gerp  
megadl 'https://mega.nz/#!eIBSkIBL!asA3K6LhJMRn98fZziviZcN6gzxwVo5Bb0tOGgyGniA'  
megadl 'https://mega.nz/#!HRYixCqS!ZxTNonx4U65J0wljdOr_KLpr51hyhJEhG8Gerpumrvs'  
megadl 'https://mega.nz/#!DZYWQIrY!Li3kQX3hugd91SE-CjUfQfTgaPoM71HuzbzdE1l-xvI'  
megadl 'https://mega.nz/#!SQIUnKBQ!ILumx5FGFiK13OaSdZdSPqdUBDqzgJTkjVJ2TBAIMf0'  
megadl 'https://mega.nz/#!nVZUCQYK!UVJFmlHDbJXJcIP2Y7gsfLxYig8zhP-Gad6jwMWMtOk'  
megadl 'https://mega.nz/#!uUA0BI7Q!zojeJvnQCDzOnvcPXq6puEC45mwOXNOt_jJlYZcEkp8'  
megadl 'https://mega.nz/#!iVIQzQSC!zTko_mLshV0evezcFjnrpDHUalLInUO2RbZdwn5gRSk'  
megadl 'https://mega.nz/#!rJAmSQJQ!NNC0ZkLPYJ8J4xRLfHkB31fYoKOIQY48F2xYab7ZMRA'  
megadl 'https://mega.nz/#!fAB2nCYB!r4uTLfY7YLJq3zqZYpRA9V6vMeIE4M0EO4aMVg-r9GA'  
megadl 'https://mega.nz/#!WMISxawD!qmm5nB7SRvBFBWimYavFjRr3NGoVph_XCtidu2Jrszk'  
megadl 'https://mega.nz/#!fRZSnSJR!QG13bTcwXVQ-2EJfSFYLcMCg1Lg-Qbh1fx9kpX6ygrY'  
megadl 'https://mega.nz/#!PMJGHCTB!LFTnLM_0fUUdg1ioA4ZOwwihQ2FLxrd1S3LHiRbnKeI'  
megadl 'https://mega.nz/#!PMYAmCJI!gR_ey1a2fFyB5QqmVsV6Z2sZtnGeuVkajQsyL4CUifM'  
megadl 'https://mega.nz/#!bUJiSAxL!1iPcCuY_Ypg-PJtVccoNvcPFJpY8X8ZVWPtBXJk5RyI'  
megadl 'https://mega.nz/#!bAJyBAIA!nlkSVNLXDw2k5oBRiYP0gX_SE22ALxkXw45CEBYNPXE'  
megadl 'https://mega.nz/#!mYJw2QxQ!YDGeFvciz-MJYpsSiXmqHsb296bPBN6IfkWjXN2Zp0U'  
megadl 'https://mega.nz/#!aRAGFCSS!33BkoJTXBB8SiPT8bTL0h_Py6cl8TItUA3_GxFVCl7g'  
megadl 'https://mega.nz/#!3YIGHYrK!eE8SYtMcwVeh5GiwiV5XfvRT0rhdFT3Pjs33S02X3Ys'  
megadl 'https://mega.nz/#!6QAEkYJL!VY1FTFofKcpbG93R13okRm42mtiuIluIhuzXR_XbVig'  
megadl 'https://mega.nz/#!XJRUgQQL!its535OWdgbX6PJtF47Y8WVKR7hUaqBEpgbUGKpSD3k'  
megadl 'https://mega.nz/#!jVAmTQpS!ujny58JnBvPc3JCK7qTn-H7bR7TG3oBOy-wt9Ryc-MY'  
megadl 'https://mega.nz/#!zUAgRSzJ!XlNsQfzWUz0c8ja5RUeItx9Jh3dPD5DZg74OVUCCWjc'  **

We downlaod hg19 FASTA reference file, both as one file and chromosome-wise file.
>**cd ${HOME}/Test/cdata/reference  
megadl 'https://mega.nz/#!6IpiFSwA!arnWQucqkfxBuDntBQK5V98MeTSKhMngM7DDUjEPMaM'  
cd ${HOME}/Test/cdata/reference/hg19  
megadl 'https://mega.nz/#!CB5TSZaR!\_4-u6dp7ksgD_89IQffOuySVA81hnv0QDJeP-fdh45E'  
megadl 'https://mega.nz/#!TZxxQLgI!Qz22I0JV3udtmoR1hwQjS-_T6iqTj43zwqirS76-B_U'  
megadl 'https://mega.nz/#!zQwhEbbJ!JwJtuQaQ6bdsFwCHkiKtWDL1VGgPMwDhQzRCoD_\_pkY'  
megadl 'https://mega.nz/#!6RwnRJLS!XP3Tch2Jp6n9kVrsOxrhrDV_m1_Vxg1KlP6NmSEsTV4'  
megadl 'https://mega.nz/#!GExz0RrC!a-s_bNBf7xZ_RuuyHyLKxePbCBCyDgzr2U4NZB151w4'  
megadl 'https://mega.nz/#!XIw11BDB!200nQ9y3V6_CFmL2cK-WXVd7jXov_vitei3JlD8DdZg'  
megadl 'https://mega.nz/#!OYoVURAA!7wiEJvuWOtJbgYYKK8_7BSXdt4Ev2y1y4Q8uLt68qDA'  
megadl 'https://mega.nz/#!aUpnBL7Y!GdsQhVIyPU5kVKG56XLNSB7ERmvN6FCJOSLI6SwmhoU'  
megadl 'https://mega.nz/#!XZpzyDLb!JjXtXybz5kBm6l8OJ6LuuCNfiLSRvya-raVrY7arVLM'  
megadl 'https://mega.nz/#!WUpH2JYT!xAtKPZQBP58uYZ02IREFd_Eu0-U5KLb819d_BYTmlME'  
megadl 'https://mega.nz/#!uVxzHbiK!j-5RgeiIuKihdwLKUYfECDMtXjtDWm4EnxBTdqwVKzI'  
megadl 'https://mega.nz/#!mYhVjJ5a!rSpAYoZ60DxvEr1I5GronIwgN7snnfj9LDsUgTwspas'  
megadl 'https://mega.nz/#!2Ip3VTjC!RPztQD22nyA0dKpsbW-nyllTtR1PRC5XvAbtdwvDFFo'  
megadl 'https://mega.nz/#!CNxDzTQQ!e4-aA99lWLIHJy8pTxPPUcvElLWx49CNODgqYkphj_I'  
megadl 'https://mega.nz/#!SMxzzRxJ!KlpqLC3m4BDRVMmQ3K-ye-gIUajw-9ZxTae38ZzX8Mk'  
megadl 'https://mega.nz/#!bM5hWJra!5I5jJp-W4AG-qaaztmF1rbvZqpc8kO_6hKMgQY7AYQE'  
megadl 'https://mega.nz/#!2YoXGRQZ!M3PwzWnpDDM3vPeTHS30LvnIzZ-uoRU0nNspgf97-xI'  
megadl 'https://mega.nz/#!DB43UZaa!EdjjnDvj4XgJwvxpSlljr5fPsTpgjLJv820nNicd2fs'  
megadl 'https://mega.nz/#!vdxjnDhT!yOYDAsLkVfuWAQi5HCzWJXDg-adGtToWSCa6pOAeuV8'  
megadl 'https://mega.nz/#!yV5zlDjZ!GmEEJtviRcASsaDuG5Z9a_-QLwDJltoYgEmgICvHPHk'  
megadl 'https://mega.nz/#!eR5HSTKS!3EUQotjQ7iiHdnI8CmFW7CFilQ_ORv4rNNWge8oG_4k'  
megadl 'https://mega.nz/#!iRgHWL6T!QLC10ns2bF0h4-3TzzcmEyVFZ11vpyjp-de24bdjRnM'  **

## Dowonloading VCF Files and Gold-standard Data ##
To demonstrate Sicario, we will download the VCF files for samples HG001 and HG002 generated by
Google's DeepVariant variant caller. We will pull the files from the publicly available google cloud bucket using gsutil. Also we will download the gold-standard variant data for samples HG001 and HG002 from "Genome in a Bottle" hosting site.

>**cd ${HOME}/Test/cdata/giab/vcf  
gsutil cp gs://deepvariant/historical_datasets/precision_fda_submission/HG001-NA12878-pFDA.vcf.gz .  
gsutil cp gs://deepvariant/historical_datasets/precision_fda_submission/HG002-NA24385-pFDA.vcf.gz .  
wget -t inf -nc ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/NA12878_HG001/NISTv3.3.2/GRCh37/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.vcf.gz  
wget -t inf -nc ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/HG002_NA24385_son/NISTv3.3.2/GRCh37/HG002_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-22_v.3.3.2_highconf_triophased.vcf.gz  **  

## Sicario Workflow ##
At this point, we need to confirm that all the required files have been downloaded. Once confirmed we can start input file preparation. We need to download sicario.jar and RTG.jar from https://bitbucket.org/islam2059/sicario/src/master/


Step 1. Preprocess reference file to create index. Please see the rtg-tools documentaion from https://github.com/RealTimeGenomics/rtg-tools
>**java -jar ${HOME}/Test/sicario/RTG.jar format ${HOME}/Test/cdata/reference/hg19.fa -o ${HOME}/Test/cdata/reference/hg19.SDF**


Step 2. Preprocess VCF files. Use command line option -vcf. Command line arguments are input VCF file name, output VCF file name and sample name.
>**java -jar ${HOME}/Test/sicario/sicario.jar -vcf ${HOME}/Test/cdata/giab/vcf/HG001-NA12878-pFDA.vcf.gz ${HOME}/Test/sicario/vcf/iDV_HG001.vcf.gz HG001  
tabix -fp vcf ${HOME}/Test/sicario/vcf/iDV_HG001.vcf.gz  
java -jar ${HOME}/Test/sicario/sicario.jar -vcf ${HOME}/Test/cdata/giab/vcf/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.vcf.gz ${HOME}/Test/sicario/vcf/hcf_HG001.vcf.gz HG001  
tabix -fp vcf ${HOME}/Test/sicario/vcf/hcf_HG001.vcf.gz  
java -jar ${HOME}/Test/sicario/sicario.jar -vcf ${HOME}/Test/cdata/giab/vcf/HG002-NA24385-pFDA.vcf.gz ${HOME}/Test/sicario/vcf/iDV_HG002.vcf.gz HG002  
tabix -fp vcf ${HOME}/Test/sicario/vcf/iDV_HG002.vcf.gz  
java -jar ${HOME}/Test/sicario/sicario.jar -vcf ${HOME}/Test/cdata/giab/vcf/HG002_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-22_v.3.3.2_highconf_triophased.vcf.gz ${HOME}/Test/sicario/vcf/hcf_HG002.vcf.gz HG002  
tabix -fp vcf ${HOME}/Test/sicario/vcf/hcf_HG002.vcf.gz**
  
Step 3. Filter only indels from VCF files. Please see the rtg-tools documentaion.
>**java -jar ${HOME}/Test/sicario/RTG.jar vcffilter -i ${HOME}/Test/sicario/vcf/iDV_HG001.vcf.gz --non-snps-only -o ${HOME}/Test/sicario/vcf/ns_iDV_HG001.vcf.gz  
java -jar ${HOME}/Test/sicario/RTG.jar vcffilter -i ${HOME}/Test/sicario/vcf/iDV_HG002.vcf.gz --non-snps-only -o ${HOME}/Test/sicario/vcf/ns_iDV_HG002.vcf.gz**

Step 4. Create true positive indels and false positive indels using gold-standard data. Please see the rtg-tools documentaion.
>**java -jar ${HOME}/Test/sicario/RTG.jar vcfeval -b ${HOME}/Test/sicario/vcf/hcf_HG001.vcf.gz -c ${HOME}/Test/sicario/vcf/ns_iDV_HG001.vcf.gz --squash-ploidy -t ${HOME}/Test/cdata/reference/hg19.SDF/ -o ${HOME}/Test/sicario/evl/ns_iDV_HG001  
java -jar ${HOME}/Test/sicario/RTG.jar vcfeval -b ${HOME}/Test/sicario/vcf/hcf_HG002.vcf.gz -c ${HOME}/Test/sicario/vcf/ns_iDV_HG002.vcf.gz --squash-ploidy -t ${HOME}/Test/cdata/reference/hg19.SDF/ -o ${HOME}/Test/sicario/evl/ns_iDV_HG002**

Step 5. Annotate both true positive indels and false positive indels using RepeatMasker annotation data. Use command line option -fnc. Command line arguments are input VCF file name, output annotated file name and functional annotation file name.
>**java -jar ${HOME}/Test/sicario/sicario.jar -fnc ${HOME}/Test/sicario/evl/ns_iDV_HG001/tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/rpt_tp.vcf.gz ${HOME}/Test/cdata/annotation/repeat.bed.gz  
java -jar ${HOME}/Test/sicario/sicario.jar -fnc ${HOME}/Test/sicario/evl/ns_iDV_HG001/fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/rpt_fp.vcf.gz ${HOME}/Test/cdata/annotation/repeat.bed.gz  
java -jar ${HOME}/Test/sicario/sicario.jar -fnc ${HOME}/Test/sicario/evl/ns_iDV_HG002/tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/rpt_tp.vcf.gz ${HOME}/Test/cdata/annotation/repeat.bed.gz  
java -jar ${HOME}/Test/sicario/sicario.jar -fnc ${HOME}/Test/sicario/evl/ns_iDV_HG002/fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/rpt_fp.vcf.gz ${HOME}/Test/cdata/annotation/repeat.bed.gz**

Step 6. Annotate both true positive indels and false positive indels using GERP score annotation data. Use command line option -grp. Command line arguments are input VCF file name, output annotated file name and folder name containing GERP score annotation file name.
>**java -jar ${HOME}/Test/sicario/sicario.jar -grp ${HOME}/Test/sicario/evl/ns_iDV_HG001/tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/grp_tp.vcf.gz ${HOME}/Test/cdata/gerp/  
java -jar ${HOME}/Test/sicario/sicario.jar -grp ${HOME}/Test/sicario/evl/ns_iDV_HG001/fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/grp_fp.vcf.gz ${HOME}/Test/cdata/gerp/  
java -jar ${HOME}/Test/sicario/sicario.jar -grp ${HOME}/Test/sicario/evl/ns_iDV_HG002/tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/grp_tp.vcf.gz ${HOME}/Test/cdata/gerp/  
java -jar ${HOME}/Test/sicario/sicario.jar -grp ${HOME}/Test/sicario/evl/ns_iDV_HG002/fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/grp_fp.vcf.gz ${HOME}/Test/cdata/gerp/**

Step 7. Annotate both true positive indels and false positive indels into indel types. Use command line option -fnc. Command line arguments are input VCF file name, output annotated file name and folder name containing chromosome-wise reference fasta file. 
>**java -jar ${HOME}/Test/sicario/sicario.jar -typ ${HOME}/Test/sicario/evl/ns_iDV_HG001/tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/typ_tp.vcf.gz ${HOME}/Test/cdata/reference/hg19/  
java -jar ${HOME}/Test/sicario/sicario.jar -typ ${HOME}/Test/sicario/evl/ns_iDV_HG001/fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/typ_fp.vcf.gz ${HOME}/Test/cdata/reference/hg19/  
java -jar ${HOME}/Test/sicario/sicario.jar -typ ${HOME}/Test/sicario/evl/ns_iDV_HG002/tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/typ_tp.vcf.gz ${HOME}/Test/cdata/reference/hg19/  
java -jar ${HOME}/Test/sicario/sicario.jar -typ ${HOME}/Test/sicario/evl/ns_iDV_HG002/fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/typ_fp.vcf.gz ${HOME}/Test/cdata/reference/hg19/**

Step 8. Annotate both true positive indels and false positive indels using DNA physical properties data. Use command line option -dna. Command line arguments are input VCF file name, output annotated file name and folder containing chromosome-wise reference fasta file.
>**java -jar ${HOME}/Test/sicario/sicario.jar -dna ${HOME}/Test/sicario/evl/ns_iDV_HG001/fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/dna_fp.vcf.gz ${HOME}/Test/cdata/reference/hg19/  
java -jar ${HOME}/Test/sicario/sicario.jar -dna ${HOME}/Test/sicario/evl/ns_iDV_HG001/tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/dna_tp.vcf.gz ${HOME}/Test/cdata/reference/hg19/  
java -jar ${HOME}/Test/sicario/sicario.jar -dna ${HOME}/Test/sicario/evl/ns_iDV_HG002/tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/dna_tp.vcf.gz ${HOME}/Test/cdata/reference/hg19/  
java -jar ${HOME}/Test/sicario/sicario.jar -dna ${HOME}/Test/sicario/evl/ns_iDV_HG002/fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/dna_fp.vcf.gz ${HOME}/Test/cdata/reference/hg19/**

Step 9. Combine both true positive indels and false positive indels woth all annotation data. Use command line option -cat. Command line arguments are all annotated file names from previous steps and combined output annotated file name.
>**java -jar ${HOME}/Test/sicario/sicario.jar -cat ${HOME}/Test/sicario/evl/ns_iDV_HG001/typ_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/dna_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/rpt_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/grp_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/typ_dna_rpt_grp_tp.vcf.gz  
java -jar ${HOME}/Test/sicario/sicario.jar -cat ${HOME}/Test/sicario/evl/ns_iDV_HG001/typ_fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/dna_fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/rpt_fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/grp_fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/typ_dna_rpt_grp_fp.vcf.gz  
java -jar ${HOME}/Test/sicario/sicario.jar -cat ${HOME}/Test/sicario/evl/ns_iDV_HG002/typ_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/dna_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/rpt_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/grp_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/typ_dna_rpt_grp_tp.vcf.gz  
java -jar ${HOME}/Test/sicario/sicario.jar -cat ${HOME}/Test/sicario/evl/ns_iDV_HG002/typ_fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/dna_fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/rpt_fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/grp_fp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/typ_dna_rpt_grp_fp.vcf.gz**

Step 10. Create training and validation input data in LIBSVM format. Use command line option -svm. Command line arguments are all annotated file names from previous steps and combined output annotated file name.
>**java -jar ${HOME}/Test/sicario/sicario.jar -svm ${HOME}/Test/sicario/evl/ns_iDV_HG001/typ_dna_rpt_grp_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG001/typ_dna_rpt_grp_fp.vcf.gz ${HOME}/Test/sicario/svm/ns_iDV_HG001.svm  
java -jar ${HOME}/Test/sicario/sicario.jar -svm ${HOME}/Test/sicario/evl/ns_iDV_HG002/typ_dna_rpt_grp_tp.vcf.gz ${HOME}/Test/sicario/evl/ns_iDV_HG002/typ_dna_rpt_grp_fp.vcf.gz ${HOME}/Test/sicario/svm/ns_iDV_HG002.svm**

Step 11. Train xgboost model using HG001 annotation data. If necessary, change training parameter in parameter file. Use command line option -trn. Command line arguments are parameter file name, training LIBSVM file name and output file name for trained model.
>**time java -jar ${HOME}/Test/sicario/sicario.jar -trn ${HOME}/Test/sicario/parameter.ini ${HOME}/Test/sicario/svm/ns_iDV_HG001.svm ${HOME}/Test/sicario/mod/ns_iDV_HG001**

Step 12. Make prediction on HG002 annotation data. Use command line option -tst. Command line arguments are test LIBSVM file name, input file name for trained model and output prediction file name.
>**time java -jar ${HOME}/Test/sicario/sicario.jar -tst ${HOME}/Test/sicario/svm/ns_iDV_HG002.svm ${HOME}/Test/sicario/mod/ns_iDV_HG001 ${HOME}/Test/sicario/prd/ns_iDV_HG001.ns_iDV_HG002**

Step 13. Calculate precision, recall and F1-score from prediction. Use command line option -auc. Command line arguments are input prediction file name, test LIBSVM file name and probability threshold for indel filtering.
>**time java -jar ${HOME}/Test/sicario/sicario.jar -auc ${HOME}/Test/sicario/prd/ns_iDV_HG001.ns_iDV_HG002 ${HOME}/Test/sicario/svm/ns_iDV_HG002.svm .5**