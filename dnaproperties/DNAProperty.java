package dnaproperties;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Vector;

/**
 * A interface that represents a property of the DNA that can be calculated from
 * the sequence. Typically these properties are stored as a simple conversation
 * tables which allow to convert each nucleotide, di-, tri- and so on
 * n-nucleotide to a numberical value. Althoug more complex models are also
 * possible
 * 
 * @author Thomas Abeel
 * 
 */
public abstract class DNAProperty {

	public static final DNAProperty ATG = new ConversionMapDNAProperty("ATG-density");

	public static final DNAProperty G = new ConversionMapDNAProperty("G-content");

	public static final DNAProperty C = new ConversionMapDNAProperty("C-content");

	public static final DNAProperty T = new ConversionMapDNAProperty("T-content");

	public static final DNAProperty A = new ConversionMapDNAProperty("A-content");

	public static final DNAProperty AT = new ConversionMapDNAProperty("AT-content");

	public static final DNAProperty GC = new ConversionMapDNAProperty("GC-content");

	public static final DNAProperty Pyrimidine = new ConversionMapDNAProperty("Pyrimidine-content");

	public static final DNAProperty Purine = new ConversionMapDNAProperty("Purine-content");

	public static final DNAProperty Bendability = new ConversionMapDNAProperty("Bendability");

	public static final DNAProperty zDNA = new ConversionMapDNAProperty("zDNA");

	public static final DNAProperty DuplexStabilityFreeEnergy = new ConversionMapDNAProperty(
			"DuplexStabilityFreeEnergy");

	public static final DNAProperty DuplexStabilityDisruptEnergy = new ConversionMapDNAProperty(
			"DuplexStabilityDisruptEnergy");

	public static final DNAProperty DNADenaturation = new ConversionMapDNAProperty("DNADenaturation");

	public static final DNAProperty PropellorTwist = new ConversionMapDNAProperty("PropellorTwist");

	public static final DNAProperty BaseStacking = new ConversionMapDNAProperty("BaseStacking");

	public static final DNAProperty ProteinDeformation = new ConversionMapDNAProperty("ProteinDeformation");

	public static final DNAProperty BendingStiffness = new ConversionMapDNAProperty("BendingStiffness");

	public static final DNAProperty ProteinDNATwist = new ConversionMapDNAProperty("ProteinDNATwist");

	public static final DNAProperty bDNATwist = new ConversionMapDNAProperty("bDNATwist");

	public static final DNAProperty APhilicity = new ConversionMapDNAProperty("APhilicity");

	public static final DNAProperty NucleosomePosition = new ConversionMapDNAProperty("NucleosomePosition");

	public static final DNAProperty DimerRadicalCleavageIntensity = new RadicalCleavageIntensity(2);

	public static final DNAProperty TrimerRadicalCleavageIntensity = new RadicalCleavageIntensity(3);

	public static final DNAProperty TetramerRadicalCleavageIntensity = new RadicalCleavageIntensity(4);

	public static final DNAProperty PentamerRadicalCleavageIntensity = new RadicalCleavageIntensity(5);

	public static final DNAProperty Twist = new ConversionMapDNAProperty("Twist");

	public static final DNAProperty Tilt = new ConversionMapDNAProperty("Tilt");

	public static final DNAProperty Roll = new ConversionMapDNAProperty("Roll");

	public static final DNAProperty Shift = new ConversionMapDNAProperty("Shift");

	public static final DNAProperty Slide = new ConversionMapDNAProperty("Slide");

	public static final DNAProperty Rise = new ConversionMapDNAProperty("Rise");

	protected DNAProperty(String name) {
		this.name = name;
	}

	private String name = null;

	/**
	 * This method will return the average value of the structural feature over
	 * the sequence given as parameter.
	 * 
	 * @param sequence
	 *            the sequence for which to calculate the average.
	 * @return the average structural value
	 */
	public abstract double value(String seq);

	/**
	 * This method will return the average value of the normalized structural
	 * feature over the sequence given as parameter.
	 * 
	 * @param sequence
	 *            the sequence for which to calculate the average.
	 * @return the average structural value
	 */
	public abstract double normalizedValue(String seq);

	/**
	 * Return the profile for the sequence given as a parameter.
	 * 
	 * @param sequence
	 *            the sequence for which to calculate the structural profile.
	 * @return the profile of the sequence
	 */
	public abstract double[] profile(String seq);

	/**
	 * Return the normalized profile for the sequence given as a parameter.
	 * 
	 * @param sequence
	 *            the sequence for which to calculate the structural profile.
	 * @return the profile of the sequence
	 */
	public abstract double[] normalizedProfile(String seq);

	public abstract int length();

	@Override
	public String toString() {
		return name;
	}

	public static DNAProperty[] values() {
		Field[] arr = DNAProperty.class.getFields();
		Vector<DNAProperty> props = new Vector<DNAProperty>();
		for (Field f : arr) {
			if (f.getType().equals(DNAProperty.class)) {
				// System.out.println(f.getName());
				try {
					DNAProperty pp = (DNAProperty) f.get(null);
					props.add(pp);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		DNAProperty[] out = new DNAProperty[props.size()];
		props.toArray(out);
		return out;
	}

	public static DNAProperty create(String name) {
		Field[] arr = DNAProperty.class.getFields();
		for (Field f : arr) {
			if (f.getType().equals(DNAProperty.class)) {
				// System.out.println(f.getName());
				try {
					DNAProperty pp = (DNAProperty) f.get(null);
					if (pp.toString().equalsIgnoreCase(name) || f.getName().equalsIgnoreCase(name))
						return pp;
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static double[] trimerDist(String name) {
		double[] d = new double[32];
		String seq = name.toUpperCase(); 
		String[][] trimerc = {
				{"AAA","TTT"},
				{"AAC","GTT"},
				{"AAG","CTT"},
				{"AAT","ATT"},
				{"ACA","TGT"},
				{"ACC","GGT"},
				{"ACG","CGT"},
				{"ACT","AGT"},
				{"AGA","TCT"},
				{"AGC","GCT"},
				{"AGG","CCT"},
				{"ATA","TAT"},
				{"ATC","GAT"},
				{"ATG","CAT"},
				{"CAA","TTG"},
				{"CAC","GTG"},
				{"CAG","CTG"},
				{"CCA","TGG"},
				{"CCC","GGG"},
				{"CCG","CGG"},
				{"CGA","TCG"},
				{"CGC","GCG"},
				{"CTA","TAG"},
				{"CTC","GAG"}, 
				{"GAA","TTC"},
				{"GAC","GTC"},
				{"GCA","TGC"},
				{"GCC","GGC"},
				{"GGA","TCC"},
				{"GTA","TAC"},
				{"TAA","TTA"},
				{"TCA","TGA"}
		};

		HashMap<String,Integer> reg = new HashMap<String,Integer>();
		HashMap<String,Integer> rmp = new HashMap<String,Integer>();

		for(int i=0; i<trimerc.length; i++){
			reg.put(trimerc[i][0], i);
			rmp.put(trimerc[i][1], i);
		}

		for (int i=0; i<seq.length()-2; i++){
			String key = seq.substring(i,i+3);
			Integer id = reg.get(key);
			if(id == null)
				id = rmp.get(key);
			if(id!=null)
				d[id]++;
		}

		for (int i=0; i<d.length; i++)
			d[i] = d[i]/(seq.length()-2);

		return d;
	}

	public static double[] dimerDist(String name) {
		double[] d = new double[8];
		String seq = name.toUpperCase(); 
		String[][] dimerc = {
				{"AA","TT"},
				{"AC","GT"},
				{"AG","CT"},
				{"AT","TA"},
				{"CA","TG"},
				{"CC","GG"},
				{"CG","GC"},
				{"GA","TC"}
		};

		HashMap<String,Integer> reg = new HashMap<String,Integer>();
		HashMap<String,Integer> rmp = new HashMap<String,Integer>();

		for(int i=0; i<dimerc.length; i++){
			reg.put(dimerc[i][0], i);
			rmp.put(dimerc[i][1], i);
		}

		for (int i=0; i<seq.length()-1; i++){
			String key = seq.substring(i,i+2);
			Integer id = reg.get(key);
			if(id == null)
				id = rmp.get(key);
			if(id!=null)
				d[id]++;
		}

		for (int i=0; i<d.length; i++)
			d[i] = d[i]/(seq.length()-1);

		return d;
	}

	public static double[] monomerDist(String name) {
		double[] d = new double[2];
		String seq = name.toUpperCase(); 
		for (int i=0; i<seq.length(); i++){
			char c = seq.charAt(i);
			if(c=='A'||c=='T')
				d[0]++;
			else
				d[1]++;
		}
		for (int i=0; i<d.length; i++)
			d[i] = d[i]/(seq.length());

		return d;
	}

	public static double[] c2N(char c) {
		double[] d = new double[4];
		char C = Character.toUpperCase(c); 
		switch(C){
		case 'A': d[0] = 1; break;
		case 'C': d[1] = 1; break;
		case 'G': d[2] = 1; break;
		case 'T': d[3] = 1; break;
		}		
		return d;
	}

	/*	
	public static double[] c2N(String c) {
		double[] d = new double[4];
		c = c.toUpperCase(); 
		for(int i=0; i<c.length(); i++){
			switch(c.charAt(i)){
			case 'A': d[0]++; break;
			case 'C': d[1]++; break;
			case 'G': d[2]++; break;
			case 'T': d[3]++; break;
			}		
		}
		return d;
	}
	 */
}
