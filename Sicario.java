import java.util.HashMap;
import util.*;

//make sure all files downloaded
//make sure 

public class Sicario {

	public static void main(String[] args) throws Exception{	

		if(args[0].equals("-vcf")) {
			String[] param = new String[3];
			param[0] = args[1]; //in
			param[1] = args[2]; //out
			param[2] = args[3]; //pid
			VCFUtils.filterFluffVCF(param); // tabix
		}
		else if(args[0].equals("-bed")) {
			String[] param = new String[2];
			param[0] = args[1]; //in
			param[1] = args[2]; //out
			param[2] = args[3]; //chr prefix
			IOUtils.addLinePrefix(param);
		}
		else if(args[0].equals("-rms")) {
			String[] param = new String[3];
			param[0] = args[1]; //in
			param[1] = args[2]; //out
			param[2] = args[3]; //pid
			VCFUtils.removeMultSample(param);
			String cmd = "tabix -fp vcf "+param[1]; //tabix must be installed in the system
			IOUtils.execBash(cmd,param[1]).waitFor();
		}
		
		else if(args[0].equals("-fnc")) {	
			String[] param = new String[3];
			param[0] = args[1]; //in 
			param[1] = args[2]; //out repeat_
			param[2] = args[3]; //anno repeat
			AnnoUtils.annoFunc(param);
		}		
		else if(args[0].equals("-grp")) {	
			String[] param = new String[3];
			param[0] = args[1]; //in 
			param[1] = args[2]; //out gerp_
			param[2] = args[3]; //gerppath
			AnnoUtils.annoScore(param);
		}			
		else if(args[0].equals("-typ")) {	
			String[] param = new String[3];
			param[0] = args[1]; //in 
			param[1] = args[2]; //out typ_
			param[2] = args[3]; //hrefpath
			AnnoUtils.annoTyp(param);
		}	
		else if(args[0].equals("-dna")) {	
			String[] param = new String[3];
			param[0] = args[1]; //in 
			param[1] = args[2]; //out dna_
			param[2] = args[3]; //hrefpath
			AnnoUtils.annoSeq(param);
		}	
		else if(args[0].equals("-cat")) {	
			String[] param = new String[args.length-1];
			for(int j=0; j<param.length; j++)
				param[j] = args[j+1]; //in1, in2, ... , out 
			IOUtils.catTSVCol(param);
		}	
		else if(args[0].equals("-svm")) {
			String[] param = new String[3];
			param[0] = args[1]; //in tp_xxxxx.tsv.gz
			param[1] = args[2]; //in fp_xxxxx.tsv.gz
			param[2] = args[3]; //out .svm
			IOUtils.csv2libSVM(param);
		}
		else if(args[0].equals("-trn")) {
			String[] param = new String[3];
			param[0] = args[1]; //in train typ_dna_repeat_gerp_ns_xxxxx.svm
			param[1] = args[2]; //in model typ_dna_repeat_gerp_ns_HG002.svm
			param[2] = args[3]; //parameter file
			XGBUtils.trainXGboost(param);
		}
		else if(args[0].equals("-tst")) {
			String[] param = new String[3];
			param[0] = args[1]; //in test typ_dna_repeat_gerp_ns_xxxxx.svm
			param[1] = args[2]; //in model typ_dna_repeat_gerp_ns_HG002.svm
			param[2] = args[3]; //in predict typ_dna_repeat_gerp_ns_HG002.svm
			XGBUtils.testXGboost(param);
		}
		else if(args[0].equals("-auc")) {
			String[] param = new String[3];
			param[0] = args[1]; //in .pred
			param[1] = args[2]; //in .svm
			param[2] = args[3]; //in threshold
			double th = Double.parseDouble(param[2]); 
			System.out.println(IOUtils.countSVM(param,th)+"\n");	
		}
		else if(args[0].equals("-tvm")) {
			String[] param = new String[3];
			param[0] = args[1]; //in .svm
			param[1] = args[2]; //outfile .svm
			param[2] = args[3]; //outpath 
			IOUtils.typifySVM(param); //in .svm	
		}

	}

}
