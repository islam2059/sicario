package util;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.apache.commons.lang.StringUtils;

import htsjdk.samtools.util.BlockCompressedOutputStream;

class PushBackLineReader {
	String lastLine = null;  
	BufferedReader reader;

	public PushBackLineReader(BufferedReader reader){
		this.reader = reader;
	}

	public String readLine() throws IOException{  
		if (lastLine != null) {  
			String temp = lastLine;  
			lastLine = null;  
			return temp;  
		} else {  
			return reader.readLine();  
		}  
	}  

	public void pushBack(String s) {  
		lastLine = s;  
	} 

	public void close() throws IOException {  
		reader.close();  
	} 
}

public class IOUtils{




	public static Process execBash(String command, String dir) throws Exception{
		ProcessBuilder pb = new ProcessBuilder();
		pb.directory(new File(dir));
		String[] cmds = {"bash", "-c", command}; 
		pb.command(cmds);
		return pb.start();       
	}


	public static BufferedReader getBR(String name) throws IOException{
		BufferedReader br = null;
		if(name.endsWith(".gz"))
			br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(name))));
		else if(name.endsWith(".bz2"))
			br = new BufferedReader(new InputStreamReader(new BZip2CompressorInputStream(new FileInputStream(name))));
		else
			br = new BufferedReader(new InputStreamReader(new FileInputStream(name)));
		return br;
	}

	public static BufferedWriter getBW(String name) throws IOException{
		BufferedWriter bw = null;
		if(name.endsWith(".gz"))
			bw = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(name))));
		else if(name.endsWith(".bz2"))
			bw = new BufferedWriter(new OutputStreamWriter(new BZip2CompressorOutputStream(new FileOutputStream(name))));
		else
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(name)));
		return bw;
	}

	public static void csv2libSVM(String[] args) throws Exception {

		String infile1 = args[0]; 
		BufferedReader br1 = getBR(infile1);
		String infile2 = args[1]; 
		BufferedReader br2 = getBR(infile2);
		String outfile = args[2]; 
		BufferedWriter bw = getBW(outfile);

		int pos = 0, neg = 0;
		String line = "";
		if(br1!=null) {
			while ((line=br1.readLine())!=null){
				pos++;
				String[] cols = StringUtils.split(line,"\t");	
				bw.write("1");
				//try {
				for(int i=0; i<cols.length; i++){
					double d = Double.parseDouble(cols[i]);
					if(d!=0) bw.write("\t"+(i+1)+":"+cols[i]);
				}	
				bw.write("\n");
				//}catch(Exception e) {
				//	System.out.println(pos);	
				//}

			}
			br1.close();
		}

		if(br2!=null) {
			while ((line=br2.readLine())!=null){
				neg++;
				String[] cols = StringUtils.split(line,"\t");	
				bw.write("0");
				for(int i=0; i<cols.length; i++){
					double d = Double.parseDouble(cols[i]);
					if(d!=0) bw.write("\t"+(i+1)+":"+cols[i]);
				}	
				bw.write("\n");
			}
			br2.close();
		}
		bw.flush();
		bw.close();
		System.out.println(args[2]+"\t"+pos+"\t"+neg);		
	}


	public static String countSVM(String[] args, double th) throws Exception {

		BufferedReader br = getBR(args[1]);
		BufferedReader brp = getBR(args[0]);
		int tp = 0, fp = 0, tn = 0, fn = 0; 

		String line = "";	
		while ((line=br.readLine())!=null){
			String str = brp.readLine();
			Double p = Double.parseDouble(str);
			if(line.startsWith("1")){
				if(p>=th) tp++;
				else fn++;
			}
			if(line.startsWith("0")){
				if(p>=th) fp++;
				else tn++;
			}

		}
		String str = args[0]+"\t"+tp+"\t"+fp+"\t"+tn+"\t"+fn+"\t"+((1.0*tp)/(tp+fp))+"\t"+((1.0*tp)/(tp+fn))+"\t"+((2.0*tp)/(2*tp+fn+fp));
		//System.out.println(str);
		//System.out.println("Precision: "+((1.0*tp)/(tp+fp)));
		//System.out.println("Recall: "+((1.0*tp)/(tp+fn)));
		//System.out.println("f1-score: "+((2.0*tp)/(2*tp+fn+fp)));
		br.close();
		return str;
	}

	public static String countSVM(String[] args) throws Exception {
		return countSVM(args, .5);
	}

	public static void typifySVM(String[] args) throws Exception{

		String svmfile = args[0];
		String outpath = args[1];
		String outfile = args[2];

		int TYP = 5;
		int[] pos = new int[TYP];
		int[] neg = new int[TYP];
		BufferedWriter[] dist = new BufferedWriter[TYP];
		for(int j=0; j<TYP; j++){
			String file = outpath+"t"+j+"_"+outfile;
			dist[j] = IOUtils.getBW(file);
		}

		String line = "";
		BufferedReader br = IOUtils.getBR(svmfile);
		while ((line=br.readLine())!=null){
			String[] cols = StringUtils.split(line,"\t");	
			int typ = Integer.parseInt(cols[2].charAt(0)+"")-2;
			dist[typ].write(line+"\n");
			if(cols[0].contains("0")) neg[typ]++;
			if(cols[0].contains("1")) pos[typ]++;
		}
		br.close();

		for(int j=0; j<TYP; j++){
			dist[j].flush();
			dist[j].close();
			System.out.println(outpath+"t"+j+"_"+outfile+"\t"+pos[j]+"\t"+neg[j]);	
		}
	}


	public static void catTSVCol(String[] args) throws Exception {

		int nbr = args.length-1; 
		BufferedReader[] br = new BufferedReader[nbr];
		for(int i=0; i<nbr; i++)
			br[i] = getBR(args[i]);

		String outfile = args[nbr]; 
		BufferedWriter bw = getBW(outfile);

		String line = "";
		while (true){
			line = br[0].readLine();
			if(line==null)
				break;
			bw.write(line);
			for(int i=1; i<nbr; i++)
				bw.write("\t"+br[i].readLine());
			bw.write("\n");
		}
		for(int i=0; i<nbr; i++)
			br[i].close();
		bw.flush();
		bw.close();
	}


	public static void addLinePrefix(String[] args) throws IOException {		

		String infile = args[0];
		String outfile = args[1];
		BufferedReader br = IOUtils.getBR(infile);
		BufferedWriter bw = IOUtils.getBW(outfile);

		String line = "";

		while ((line = br.readLine()) != null){
			if(!line.startsWith(args[2]))
				bw.write(args[2]+line+"\n");
		}
		br.close();				
		bw.flush();
		bw.close();				
	}





















	public static String printMatrix(float[][] d){
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<d.length; i++){
			for(int j=0; j<d[i].length; j++)
				sb.append("\t"+d[i][j]);
			sb.append("\n");
		}
		return new String(sb);
	}





	public static <K,V> String printMap(Map<K,V> m){
		StringBuffer sb = new StringBuffer();
		for(Map.Entry<K,V> entry: m.entrySet()){
			K key = entry.getKey();
			V val = entry.getValue();
			sb.append(key+"\t"+val+"\n");
		}
		//sb.append("\n");
		return new String(sb);
	}	


	public static void procGERP(String[] args) throws IOException{
		
		String path = args[0];
		String name = args[1];
		BufferedReader br = getBR(path+name);
		BufferedWriter[] bw = new BufferedWriter[22];
		for(int j=0; j<22; j++)
			bw[j] = getBW(path+(j+1)+"_"+name);

		int[] m = new int[22]; 

		int n = 0;
		String line = "";
		while ((line = br.readLine()) != null){	
			n++; if(n%1000000==0) System.out.println(n);
			if (line.startsWith("#"))
				continue;
			Scanner sc = new Scanner(line);					
			String CHROM = sc.next();	
			int chr;
			if(StringUtils.isNumeric(CHROM))
				chr = Integer.parseInt(CHROM);	
			else if(CHROM.startsWith("chr") && StringUtils.isNumeric(CHROM.substring(3)))
				chr = Integer.parseInt(CHROM.substring(3));	
			else
				continue;

			/////////Only for GERP file////////
			int loc = Integer.parseInt(sc.next()); sc.next(); sc.next();
			String grp = sc.next();

			while(m[chr-1]<loc){
				bw[chr-1].write("0\n");
				m[chr-1]++;
			}
			bw[chr-1].write(grp+"\n");
			m[chr-1]++;
			sc.close();
		}
		br.close();
		for(int j=0; j<22; j++){
			while(m[j]<SeqUtils.CLEN[j]){
				bw[j].write("0\n");
				m[j]++;
			}
			bw[j].flush();
			bw[j].close();
		}
	}

	public static void procRMSK(String[] args) throws Exception {

		args = new String[4];
		args[0] = ".";
		args[1] = "rmsk.txt.gz";

		String path = args[0];
		String name = args[1];
		BufferedReader br = getBR(path+name);

		int[] nc = {5,6,7,11};
		String outfile = "repeat.bed.gz";
		BufferedWriter bw = getBW(path+outfile);

		ArrayList<TreeSet<String>> Ts = new ArrayList<TreeSet<String>>();
		for(int i=0; i<nc.length-3; i++)
			Ts.add(new TreeSet<String>());

		int n=0;		
		String line = "";
		while ((line = br.readLine()) != null){
			n++; if(n%100000==0) System.out.println(n);
			String[] cols = StringUtils.split(line,"\t\n");	
			String chr = cols[nc[0]];
			if(!StringUtils.isNumeric(chr.substring(3)))
				continue;	
			StringBuffer sb = new StringBuffer("");
			sb.append(chr);
			for(int i=1; i<nc.length; i++)
				sb.append("\t"+cols[nc[i]].toUpperCase());
			bw.write(sb.substring(0,sb.length())+"\n");
			bw.flush();
		}
		
		br.close();		
		bw.flush();
		bw.close();

	}
}