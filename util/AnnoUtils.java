package util;
import htsjdk.samtools.util.IntervalTree;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import dnaproperties.DNAProperty;

public class AnnoUtils{


	public static void annoScore(String[] args) throws Exception {
		String infile = args[0];
		String outfile = args[1];
		String gerppath = args[2];

		DecimalFormat df = new DecimalFormat("#.#####");
		int SPAN = 10;
		//Reading variant file
		PushBackLineReader br = new PushBackLineReader(IOUtils.getBR(infile));
		BufferedWriter bw = IOUtils.getBW(outfile);

		String line = "";
		double scale = 1000000.0;

		int[] gerp = new int[250000000];
		for(int j=0; j<22; j++){
			int chr = j+1;
			infile = gerppath+chr+"_gerp_whole_genome.bed.gz";
			System.out.println(infile);
			PushBackLineReader brf = new PushBackLineReader(IOUtils.getBR(infile));
			int cur = 0;
			while((line = brf.readLine()) != null)
				gerp[cur++] = (int) Math.round(scale*Double.parseDouble(line));
			brf.close();

			int n=0;
			while ((line = br.readLine()) != null){
				if(line.startsWith("#"))
					continue;
				n++; if(n%100000==0) System.out.println(n);
				String[] cols = StringUtils.split(line,"\t\n ");
				if(!cols[0].equalsIgnoreCase("chr"+chr) && !cols[0].equalsIgnoreCase(""+chr)){
					br.pushBack(line);
					break;
				}	
				int loc = Integer.parseInt(cols[1])-1; //making 0-based
				String ref = cols[3];

				StringBuffer str = new StringBuffer("");
				str.append((gerp[loc-SPAN]/scale));
				for(int i=loc-SPAN+1; i<=loc; i++)
					str.append("\t"+(gerp[i]/scale));

				double sum = 0;
				for(int i=loc; i<loc+ref.length(); i++)
					sum += gerp[i]/scale;
				str.append("\t"+df.format(sum/ref.length()));

				for(int i=loc+ref.length()-1; i<loc+ref.length()+SPAN; i++)
					str.append("\t"+(gerp[i]/scale));

				bw.write(str+"\n");
			}			
		}
		br.close();		
		bw.flush();
		bw.close();
	}

	public static void annoTyp(String[] args) throws Exception {

		String infile = args[0];	
		String outfile = args[1];
		String fastapath = args[2];

		//Reading variant file
		PushBackLineReader br = new PushBackLineReader(IOUtils.getBR(infile));
		BufferedWriter bw = IOUtils.getBW(outfile);

		String line = "";
		byte[] chrom = new byte[250000000];
		for(int j=0; j<22; j++){
			int chr = j+1;
			infile = fastapath+"chr"+chr+".fa";
			BufferedReader brf = new BufferedReader(new InputStreamReader(new FileInputStream(infile)));
			if(!StringUtils.isNumeric(brf.readLine().substring(4))) continue;
			int cur = 0;
			while((line = brf.readLine()) != null){
				char[] c = line.toCharArray();
				for(int i=0; i<c.length; i++)
					chrom[cur++] = (byte)c[i];
			}	
			brf.close();

			while ((line = br.readLine()) != null){
				if(line.startsWith("#"))
					continue;
				String[] cols = StringUtils.split(line,"\t\n ");
				if(!cols[0].equalsIgnoreCase("chr"+chr) && !cols[0].equalsIgnoreCase(""+chr)){
					br.pushBack(line);
					break;
				}	
				int loc = Integer.parseInt(cols[1]);
				String ref = cols[3];
				String alt = cols[4];

				MutableInt mpos = new MutableInt(loc);	
				ArrayList<String> var = new ArrayList<String>();
				var.add(ref); 
				var.add(alt);
				var = SeqUtils.removeCommonFix(var,mpos,true);	
				var = SeqUtils.normHMP(var,mpos,chrom);
				int[] typ = new int[5]; //BSUB:0; HMP:1; RPT:2, IDL:3, USUB:4  
				int t = SeqUtils.getType(var.get(0),var.get(1),mpos.intValue(),chrom);
				int dist = SeqUtils.lDist(var.get(0), var.get(1));
				typ[t] = 1;
				String str = ""+dist;
				for(int i=0; i<typ.length; i++)
					str += "\t"+typ[i];		
				bw.write(str+"\n");
			}
		}

		br.close();		
		bw.flush();
		bw.close();
	}


	public static void annoSeq(String[] args) throws Exception {

		String infile = args[0];
		String outfile = args[1];
		String fastapath = args[2];

		//Reading variant file
		PushBackLineReader br = new PushBackLineReader(IOUtils.getBR(infile));
		BufferedWriter bw = IOUtils.getBW(outfile);

		String line = "";

		byte[] chrom = new byte[250000000];
		for(int j=0; j<22; j++){
			int chr = j+1;
			infile = fastapath+"chr"+chr+".fa";
			BufferedReader brf = new BufferedReader(new InputStreamReader(new FileInputStream(infile)));
			if(!StringUtils.isNumeric(brf.readLine().substring(4))) continue;
			int cur = 0;
			while((line = brf.readLine()) != null){
				char[] c = line.toCharArray();
				for(int i=0; i<c.length; i++)
					chrom[cur++] = (byte)c[i];
			}	
			brf.close();

			while ((line = br.readLine()) != null){
				if(line.startsWith("#"))
					continue;
				String[] cols = StringUtils.split(line,"\t\n ");
				if(!cols[0].equalsIgnoreCase("chr"+chr) && !cols[0].equalsIgnoreCase(""+chr)){
					br.pushBack(line);
					break;
				}	
				int loc = Integer.parseInt(cols[1]);
				String ref = cols[3];
				String alt = cols[4];
				int len = 100;
				if(loc-1-len<0) len = loc-1;
				String pfix = new String(chrom,loc-1-len,len);
				String sfix = new String(chrom,loc-1+ref.length(),len);		


				DecimalFormat df = new DecimalFormat("#.###");
				String str = ref.length()+"\t"+alt.length();

				String seq10 = "";
				String seq50 = "";
				String seq100 = "";

				try{
					seq10 = pfix.substring(pfix.length()-10)+ref+sfix.substring(0,10);
					seq50 = pfix.substring(pfix.length()-50)+ref+sfix.substring(0,50);
					seq100 = pfix+ref+sfix;
				}catch(Exception e){
					e.printStackTrace();
					BufferedReader b  = new BufferedReader(new InputStreamReader(System.in));
					b.readLine();
				}

				DNAProperty[] dp = DNAProperty.values();
				double[] d = new double[dp.length];
				for(int i=0; i<dp.length; i++){
					d[i] = dp[i].normalizedValue(seq10);
					str += "\t"+df.format(d[i]);
				}
				d = new double[dp.length];
				for(int i=0; i<dp.length; i++){
					d[i] = dp[i].normalizedValue(seq50);
					str += "\t"+df.format(d[i]);
				}

				d = DNAProperty.trimerDist(seq100);
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);

				d = DNAProperty.c2N(pfix.charAt(pfix.length()-5));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(pfix.charAt(pfix.length()-4));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(pfix.charAt(pfix.length()-3));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(pfix.charAt(pfix.length()-2));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(pfix.charAt(pfix.length()-1));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);

				d = DNAProperty.c2N(sfix.charAt(0));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(sfix.charAt(1));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(sfix.charAt(2));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(sfix.charAt(3));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(sfix.charAt(4));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);


				d = DNAProperty.c2N(ref.charAt(0));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(ref.charAt(ref.length()-1));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.monomerDist(ref);
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				if(ref.length()>1)
					d = DNAProperty.dimerDist(ref);
				else
					d = new double[8];
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);

				d = DNAProperty.c2N(alt.charAt(0));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.c2N(alt.charAt(alt.length()-1));
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				d = DNAProperty.monomerDist(alt);
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);
				if(alt.length()>1)
					d = DNAProperty.dimerDist(alt);
				else
					d = new double[8];
				for(int i=0; i<d.length; i++)
					str += "\t"+df.format(d[i]);

				bw.write(str+"\n");
			}
		}

		br.close();		
		bw.flush();
		bw.close();
	}


	public static void annoFunc(String[] args) throws IOException {

		String infile = args[0];
		String outfile = args[1];
		String annofile = args[2];
		BufferedWriter bw = IOUtils.getBW(outfile);

		//Reading variant file
		PushBackLineReader vr = new PushBackLineReader(IOUtils.getBR(infile));
		PushBackLineReader ar = new PushBackLineReader(IOUtils.getBR(annofile));

		TreeSet<String> fset = new TreeSet<String>();		
		String line = "";
		while ((line = ar.readLine()) != null){
			String[] cols = StringUtils.split(line,"\t\n ");
			String[] f = StringUtils.split(cols[3],";");
			for(int j=0; j<f.length; j++)
				fset.add(f[j]);
		}
		System.out.println("Features Loaded");
		ar.close();

		ar = new PushBackLineReader(IOUtils.getBR(annofile));
		ArrayList<String> flist = new ArrayList<String>(fset);
		line = "CHR\tLOC\tREF\tALT";
		for(int i=0; i<flist.size(); i++)
			line += ("\t"+flist.get(i));
		//bw.write(line+"\n"); bw.flush();bw.close();

		TreeMap<String,IntervalTree> feature = new TreeMap<String,IntervalTree>();
		for(int i=0; i<22; i++){
			int chr = i+1;
			while ((line = ar.readLine()) != null){
				String[] cols = StringUtils.split(line,"\t\n ");
				if(!cols[0].equalsIgnoreCase("chr"+chr) && !cols[0].equalsIgnoreCase(""+chr)){
					ar.pushBack(line);
					break;
				}

				String[] f = StringUtils.split(cols[3],";");
				for(int j=0; j<f.length; j++){
					IntervalTree b = feature.remove(f[j]);
					if(b==null)
						b = new IntervalTree();
					b.put(Integer.parseInt(cols[1]),Integer.parseInt(cols[2]),null);
					feature.put(f[j],b);
				}
			}
			System.out.println("chr "+(i+1)+" Annotation Loaded");

			while ((line = vr.readLine()) != null){
				if(line.startsWith("#"))
					continue;
				String[] cols = StringUtils.split(line,"\t\n ");
				if(!cols[0].equalsIgnoreCase("chr"+chr) && !cols[0].equalsIgnoreCase(""+chr)){
					vr.pushBack(line);
					break;
				}	
				int pos = Integer.parseInt(cols[1]);
				int end = Integer.parseInt(cols[1])+cols[3].length();

				StringBuffer str = new StringBuffer("");
				//str = chr+"\t"+pos+"\t"+cols[3]+"\t"+cols[4];

				IntervalTree b = feature.get(flist.get(0));
				if(b!=null && b.minOverlapper(pos,end)!=null)
					str.append("1");
				else
					str.append("0");

				for(int j=1; j<flist.size(); j++){
					b = feature.get(flist.get(j));
					if(b!=null && b.minOverlapper(pos,end)!=null)
						str.append("\t1");
					else
						str.append("\t0");
				}
				bw.write(str.toString()+"\n");
			}
		}
		vr.close();
		ar.close();
		bw.flush();
		bw.close();
	}

}
