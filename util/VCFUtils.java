package util;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;

public class VCFUtils{

	public static int NCOL = 1;
	public static int ATTR = 10;
	public String[] cols;
	public int err = 0;


	public static void removeMultSample(String[] args) throws Exception {		

		String infile = args[0];
		String outfile = args[1];
		String pid = args[2];
		BufferedReader br = IOUtils.getBR(infile);
		BufferedWriter bw = IOUtils.getBW(outfile);
		String line = "";

		bw.write("##fileformat=VCFv4.2\n" + 
				"##reference=hg19\n" +
				"#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t"+pid+"\n");
		//"#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n");

		while ((line = br.readLine()) != null){
			if(line.startsWith("#"))
				continue;
			Scanner sc = new Scanner(line);
			String chr = sc.next();
			String pos = sc.next();
			String id = sc.next();
			String ref = sc.next();
			String alt = sc.next();
			String qlt = sc.next();
			String flt = sc.next();
			String inf = sc.next();
			sc.close();
			
			if(inf.contains("BASE=FN"))
				continue;
	
			String tmp = chr+"\t"+pos+"\t"+id+"\t"+ref+"\t"+alt+"\t"+qlt+"\t"+flt+"\t.\tGT\t1/1";
			bw.write(tmp+"\n");
		}
		br.close();				
		bw.flush();
		bw.close();		
		
		String cmd = "tabix -fp vcf "+outfile;
		IOUtils.execBash(cmd,"/").waitFor();
		
	}

	
	

	public static void filterFluffVCF(String[] args) throws IOException {		

		String infile = args[0];
		String outfile = args[1];
		String pid = args[2];
		BufferedReader br = IOUtils.getBR(infile);
		BufferedWriter bw = IOUtils.getBW(outfile);

		int n = 0;
		String line = "";

		bw.write("##fileformat=VCFv4.2\n" + 
				"##reference=hg19\n" +
				"#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t"+pid+"\n");
		//"#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n");

		while ((line = br.readLine()) != null){
			if(line.startsWith("#"))
				continue;
			n++; if(n%1000000==0) System.out.println(n);

			//  0   1  2   3   4    5      6    7      8 9+0-68    
			//CHR POS ID REF ALT QUAL FILTER INFO FORMAT 69-IND
			//GT:PS:FT:HQ:EHQ:CGA_CEHQ:GL:CGA_CEGL:DP:AD:CGA_RDP

			String regex = "[ACGTacgt]+";
			Scanner sc = new Scanner(line);
			String chr = sc.next();
			String pos = sc.next();
			String id = sc.next();
			String ref = sc.next();
			String alt = sc.next();
			String qlt = sc.next();
			String flt = sc.next();
			String inf = sc.next();
			String fmt = sc.next();
			String rst = sc.nextLine();
			sc.close();
			if(chr.length()<4)
				chr = "chr"+chr;
			if(!StringUtils.isNumeric(chr.substring(3))) //autosome
				continue;
			if(!StringUtils.isNumeric(pos))
				continue;			
			if(!ref.matches(regex) || !alt.matches(regex))
				continue;

			MutableInt mpos = new MutableInt(Integer.parseInt(pos));

			if(mpos.intValue()<10000)
				continue;

			ArrayList<String> var = new ArrayList<String>();
			if(!(ref.length()==1 && alt.length()==1)){ //remove common prefix/suffix
				var.add(ref);
				String[] alts = StringUtils.split(alt,",");
				for(int i=0; i<alts.length; i++)
					var.add(alts[i]);

				var = SeqUtils.removeCommonFix(var, mpos, true);
				ref = var.get(0);
				alt = var.get(1);
				for(int i=2; i<var.size(); i++)
					alt += ","+var.get(i);
			}		

			String tmp = chr+"\t"+mpos.intValue()+"\t"+id+"\t"+ref.toUpperCase()+"\t"+alt.toUpperCase()+"\t"+qlt+"\t"+flt+"\t"+inf+"\t"+fmt+rst;
			bw.write(tmp+"\n");
		}
		br.close();				
		bw.flush();
		bw.close();				
	}
	

}







