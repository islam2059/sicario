package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import ml.dmlc.xgboost4j.java.Booster;
import ml.dmlc.xgboost4j.java.DMatrix;
import ml.dmlc.xgboost4j.java.XGBoost;

public class XGBUtils {
	
	public static void crossValidXGboost(String[] args) throws Exception {
		crossValidXGboost(args, null, 250);
	}
	
	public static void crossValidXGboost(String[] args, HashMap<String, Object> params, int round) throws Exception {

		String trainFile = args[0];
		System.out.println(trainFile);
		DMatrix trainMat = new DMatrix(trainFile);		

	    int nfold = 5;
	    //set additional eval_metrics
	    String[] metrics = {"rmse","error","auc"};
	    String[] evalHist = XGBoost.crossValidation(trainMat, params, round, nfold, metrics, null,null);
		for(int i=0; i<evalHist.length; i++)
			System.out.println(evalHist[i]);
	}
	
	public static void testXGboost(String[] args) throws Exception {
		
		String testFile = args[0];
		String modelFile = args[1]+".model";		
		String predictFile = args[2]+".pred";
		DMatrix testMat = new DMatrix(testFile);		
		Booster booster = null;
		booster = XGBoost.loadModel(modelFile);
		float[][] predicts = booster.predict(testMat);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(predictFile)));
		bw.write(IOUtils.printMatrix(predicts));
		bw.flush();
		bw.close();
	}	

	public static void trainXGboost(String[] args) throws Exception {

		String paramFile = args[0];
		String trainFile = args[1];
		String modelFile = args[2]+".model";		
		String featureFile = args[2]+".feat";
		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("eval_metric", "auc");
		params.put("objective", "binary:logistic");
		params.put("nthread", 1);
		params.put("silent", 0);
		params.put("eta", .25);
		params.put("max_depth", 16);
		params.put("gamma", 1.0);
		int round = 100;
		
		BufferedReader br = IOUtils.getBR(paramFile);
		String line = "";
		while ((line = br.readLine()) != null){
			String[] cols = StringUtils.split(line,"\t\n");
			if(cols[0].equalsIgnoreCase("round")) {
				round = Integer.parseInt(cols[1]);
				continue;
			}
			params.put(cols[0], cols[1]);
			System.out.println(params.get(cols[0]));
		}
		br.close();	
	
		
		DMatrix trainMat = new DMatrix(trainFile);
		HashMap<String, DMatrix> watches = new HashMap<String, DMatrix>();
		watches.put("train", trainMat);
		Booster booster = null;
		booster = XGBoost.train(trainMat, params, round, watches, null, null);
		booster.saveModel(modelFile);
		Map<String, Integer> fs = booster.getFeatureScore("");
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(featureFile)));
		bw.write(IOUtils.printMap(fs)+"\n");
		bw.flush();
		bw.close();
	}
	
	public static boolean checkPredicts(float[][] fPredicts, float[][] sPredicts) {
		if (fPredicts.length != sPredicts.length) {
			return false;
		}

		for (int i = 0; i < fPredicts.length; i++) {
			if (!Arrays.equals(fPredicts[i], sPredicts[i])) {
				return false;
			}
		}

		return true;
	}

	public static void saveDumpModel(String modelPath, String[] modelInfos) throws IOException {
		try{
			PrintWriter writer = new PrintWriter(modelPath, "UTF-8");
			for(int i = 0; i < modelInfos.length; ++ i) {
				writer.print("booster[" + i + "]:\n");
				writer.print(modelInfos[i]);
			}
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
