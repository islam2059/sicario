package util;


import java.util.*;

import org.apache.commons.lang.mutable.MutableInt;

class SeqLenComparator implements Comparator<String>{
	public int compare(String s1, String s2) {
		return s1.length()-s2.length();
	}
}

public class SeqUtils {

	public static String[] typestr = {"HMP","REP","SNV","IDL","TRE","DRE","RST"};

	public static int[] CLEN = {
			249250621,
			243199373,
			198022430,
			191154276,
			180915260,
			171115067,
			159138663,
			146364022,
			141213431,
			135534747,
			135006516,
			133851895,
			115169878,
			107349540,
			102531392,
			90354753,
			81195210,
			78077248,
			59128983,
			63025520,
			48129895,
			51304566
	};


	
	public static ArrayList<String> removeCommonFix(ArrayList<String> var, MutableInt pos, boolean cxt){ //remove common pre/suffix
		int min = var.indexOf(Collections.min(var,new SeqLenComparator()));
		int cl = cxt?1:0;

		while(var.get(min).length()>cl){ // 1/0 keep one common fix or not
			boolean sim = true;
			for(int i=0; (i<var.size()-1) && sim; i++)
				sim &= var.get(i).charAt(0) == var.get(i+1).charAt(0);
			if(sim==false) break;
			//this.pre = this.pre+""+var.get(0).charAt(0);
			for(int i=0; i<var.size(); i++)
				var.set(i,var.get(i).substring(1));
			pos.increment();
		}

		while(var.get(min).length()>cl){
			boolean sim = true;
			for(int i=0; (i<var.size()-1) && sim; i++)
				sim &= var.get(i).charAt(var.get(i).length()-1) == var.get(i+1).charAt(var.get(i+1).length()-1);
			if(sim==false) break;
			//this.suf = ""+var.get(0).charAt(var.get(0).length()-1)+this.suf;
			for(int i=0; i<var.size(); i++)
				var.set(i,var.get(i).substring(0,var.get(i).length()-1));
		}	
		return var;		
	}

	// SNP, BSUB, HMP(normalized), HMP(non-normalized, should not be any), , USUB 

	public static int getType(String ref, String alt, int loc, byte[] chrom){
		int len = (alt.length()*3>200)?alt.length()*3:200;
		if(loc-1-len<0) len = loc-1;
		String pfix = new String(chrom,loc-1-len,len);
		String sfix = new String(chrom,loc-1+ref.length(),len);		
		return getType(ref, alt, pfix, sfix);
	}


	public static int getType(String ref, String alt, String pfix, String sfix){ //Used in classify, assumed same case
		int typ = 0;
		String s1 = ref.length()<=alt.length()?ref:alt; //s1 smaller/equal
		String s2 = ref.length()<=alt.length()?alt:ref;
		char p = pfix.charAt(pfix.length()-1);
		char s = sfix.charAt(0);
		char q = 0, r = 0;
		String diff = "", pfx = "", sfx = "";
		if(s2.startsWith(s1)){    // C  CAT
			diff = s2.substring(s1.length());
			q = s2.charAt(s1.length()-1); 
			r = s;	
			pfx = pfix+s1;
			sfx = sfix;
		}
		if(s2.endsWith(s1)){
			diff = s2.substring(0,s2.length()-s1.length());
			q = p;
			r = s2.charAt(s2.length()-s1.length());
			pfx = pfix;
			sfx = s1+sfix;
		}
		int d = lDist(s1,s2);

		if(s1.length()==s2.length())
			typ = (s1.length()==1)?-1:0; //SNP:BSUB
		else if(diff.length()>0){ //Indel 
			if((SeqUtils.isHomopolymer(diff) || diff.length()==1) && (q==diff.charAt(0) || r==diff.charAt(diff.length()-1)))
				typ = 1;	//HMP																																		
			else if(diff.length()>1 && (pfx.endsWith(diff) || sfx.startsWith(diff)))
				typ = 2;	//RPT
			else
				typ = 3;
		}
		else 
			typ = 4;
		return typ;
	}

	public static int bDist(char a, char b){
		return (a==b)?0:1;
	}	
	
	public static int lDist(String s1, String s2) {
		int l1 = s1.length();
		int l2 = s2.length();
		//int l  = l1<l2? l1:l2;
		int[][] d = new int[l1+1][l2+1];

		//for (int i=0; i<=l; i++)
		//d[i][0] = d[0][i] = i;

		for (int i=0; i<=l1; i++)
			d[i][0] = i;
		for (int j=0; j<=l2; j++)
			d[0][j] = j;			

		for (int i=1; i<=l1; i++)
			for (int j=1; j<=l2; j++)
				d[i][j] = (int) Math.min(Math.min(d[i-1][j]+1, d[i][j-1]+1), d[i-1][j-1]+bDist(s1.charAt(i-1),s2.charAt(j-1)));

		return d[l1][l2];
	}
	
	public static ArrayList<String> normHMP(ArrayList<String> var, MutableInt pos, byte[] chrom){

		ArrayList<String> tmp = new ArrayList<String>(var);
		Collections.sort(tmp,new SeqLenComparator());
		
		if(!isHomopolymer(tmp,0))
			return var;

		int i = pos.intValue()-1;
		byte t = (byte) Character.toUpperCase(chrom[i]); 
		while(var.get(0).charAt(0)==Character.toUpperCase(chrom[i])){
			i--;
			pos.decrement();
		}

		for(int j=0; j<var.size(); j++)
			var.set(j, Character.toUpperCase((char)chrom[i])+var.get(j).substring(1));

		return var;

	}


	public static boolean isHomopolymer(String s) {
		if(s.length()<2)
			return false;	
		boolean isHMP = true;
		for(int i=0; i<s.length()-1; i++)
			isHMP &= s.charAt(i)==s.charAt(i+1);
		return isHMP;
	}




	public static int[] translateDNA(String s){
		int[] code = new int[s.length()];
		for(int i=0; i<s.length(); i++){
			if(s.charAt(i)=='A' || s.charAt(i)=='a')
				code[i] = 0;
			else if(s.charAt(i)=='C' || s.charAt(i)=='c')
				code[i] = 1;
			else if(s.charAt(i)=='G' || s.charAt(i)=='g')
				code[i] = 2;
			else if(s.charAt(i)=='T' || s.charAt(i)=='t')
				code[i] = 3;
			else 
				code[i] = 4;
		}	
		return code;
	}


	public static int[] translateAA(String s){
		int[] code = new int[s.length()];
		for(int i=0; i<s.length(); i++){
			if(s.charAt(i)=='A')
				code[i] = 0;
			else if(s.charAt(i)=='R')
				code[i] = 1;
			else if(s.charAt(i)=='N')
				code[i] = 2;
			else if(s.charAt(i)=='D')
				code[i] = 3;
			else if(s.charAt(i)=='C')
				code[i] = 4;
			else if(s.charAt(i)=='Q')
				code[i] = 5;
			else if(s.charAt(i)=='E')
				code[i] = 6;
			else if(s.charAt(i)=='G')
				code[i] = 7;
			else if(s.charAt(i)=='H')
				code[i] = 8;
			else if(s.charAt(i)=='I')
				code[i] = 9;
			else if(s.charAt(i)=='L')
				code[i] = 10;
			else if(s.charAt(i)=='K')
				code[i] = 11;
			else if(s.charAt(i)=='M')
				code[i] = 12;
			else if(s.charAt(i)=='F')
				code[i] = 13;
			else if(s.charAt(i)=='P')
				code[i] = 14;
			else if(s.charAt(i)=='S')
				code[i] = 15;
			else if(s.charAt(i)=='T')
				code[i] = 16;
			else if(s.charAt(i)=='W')
				code[i] = 17;
			else if(s.charAt(i)=='Y')
				code[i] = 18;
			else if(s.charAt(i)=='V')
				code[i] = 19;
		}	
		return code;
	}	

	public static String removeCharPair(String hg, String pt, char c){
		char[] chg = new char[hg.length()];
		char[] cpt = new char[pt.length()];

		int j=0;
		int n = hg.length()>pt.length()?pt.length():hg.length();
		for(int i=0; i<n; i++){
			if(hg.charAt(i)==c && pt.charAt(i)==c)
				continue;
			chg[j] = hg.charAt(i);
			cpt[j] = pt.charAt(i);
			j++;
		}
		String shg = new String(chg,0,j);
		String spt = new String(cpt,0,j);
		if(hg.length()>pt.length())
			shg += hg.substring(n);
		if(pt.length()>hg.length())
			spt += pt.substring(n);
		return (shg+"\n"+spt);
	}

	public static int indexDash(String hg, int idx){
		int n = hg.length();
		if(idx<0||idx>n)
			return -1;
		int i=0,j=0;
		while(i<n && j!=idx){
			if(hg.charAt(i)!='-')
				j++;
			i++;
		}
		return i;
	}


	public static boolean isHomopolymer(ArrayList<String> var, double frac) {
		int mut = 0;
		int len = 0;
		for(int i=0; i<var.size()-1; i++){
			mut += mutStartsWith(var.get(i+1),var.get(i));	
			String str = var.get(i+1).substring(var.get(0).length());
			mut += mutHomopolymer(str);
			len += str.length()-1;
		}
		if(mut<=frac*len)
			return true;

		mut = 0;
		len = 0;
		for(int i=0; i<var.size()-1; i++){
			mut += mutEndsWith(var.get(i+1),var.get(i));	
			String str = var.get(i+1).substring(0, var.get(i+1).length()-var.get(0).length());
			mut += mutHomopolymer(str);
			len += str.length()-1;
		}
		if(mut<=frac*len)
			return true;

		else 
			return false;
	}

	public static boolean hasFIX(ArrayList<String> var, int lnode) {
		boolean isFIX = false;
		for(int i=0; i<var.size() && !isFIX; i++)
			if(i!=lnode)
				isFIX |=(var.get(i).startsWith(var.get(lnode))
						|| var.get(lnode).startsWith(var.get(i))
						|| var.get(i).endsWith(var.get(lnode))
						|| var.get(lnode).endsWith(var.get(i))
						|| var.get(i).contains(var.get(lnode))
						|| var.get(lnode).contains(var.get(i)));
		return isFIX;
	}	

	public static boolean isPREFIX(ArrayList<String> var) {
		boolean isPREFIX = true;
		for(int i=0; i<var.size()-1; i++)
			isPREFIX &= var.get(i+1).startsWith(var.get(i));
		return isPREFIX;
	}

	public static boolean isKNP(ArrayList<String> var, int k) {	
		boolean isKNP = true;
		for(int i=0; i<var.size() && isKNP; i++)
			isKNP &= var.get(i).length()<=k && var.get(i).length()>0;
			return isKNP;
	}
	
	public static boolean isNNP(ArrayList<String> var, int k) {	
		if(var.size()<2)
			return false;
		boolean isNNP = true; 
		for(int i=0; i<var.size()-1 && isNNP; i++)
			isNNP &= var.get(i).length()==var.get(i+1).length() && var.get(i).length()==k;
		return isNNP;
	}

	public static boolean isNNP(ArrayList<String> var) {	
		if(var.size()<2)
			return false;
		boolean isNNP = true; 
		for(int i=0; i<var.size()-1 && isNNP; i++)
			isNNP &= var.get(i).length()==var.get(i+1).length();
		return isNNP;
	}

	public static boolean isREP(ArrayList<String> var, double frac) {	
		boolean isREP = false;
		int start = var.get(0).length();
		String last = var.get(var.size()-1);			
		for(int w=2; w<9; w++){
			if(last.length()>=start+w){

				String prd = last.substring(start,start+w);

				int mut = 0;
				int len = 0;
				for(int i=1; i<var.size(); i++){
					StringBuffer sb = new StringBuffer();	
					int rep = var.get(i).length()-var.get(0).length();
					sb.append(var.get(0));
					int cyc = rep/prd.length();
					int rem = rep%prd.length();
					for(int j=0; j<cyc; j++)
						sb.append(prd);
					sb.append(prd.substring(0, rem));				
					mut += lDist(var.get(i), sb.toString());
					len += rep;
				}
				if(mut<=len*frac){		
					isREP = true;
					break;
				}
			}
		}
		return isREP;
	}

	public static int mutStartsWith(String s1, String s2) {
		int len = Math.min(s1.length(), s2.length());		
		int mut = 0;
		for(int i=0; i<len; i++)
			mut += s1.charAt(i)==s2.charAt(i)?0:1;
		return mut;
	}	

	public static int mutEndsWith(String s1, String s2) {
		int len = Math.min(s1.length(), s2.length());		
		int mut = 0;
		for(int i=1; i<=len; i++)
			mut += s1.charAt(s1.length()-i)==s2.charAt(s2.length()-i)?0:1;
		return mut;
	}	

	public static int mutHomopolymer(String s) {
		int mut = 0;
		for(int i=0; i<s.length()-1; i++){
			mut += s.charAt(i)==s.charAt(i+1)?0:1;
		}
		return mut;
	}




	

		

	public static String cc(String s){
		char[] c = s.toCharArray();
		for(int i=0; i<c.length; i++){
			switch(c[i]){
			case 'a': c[i]='t'; break;
			case 'c': c[i]='g'; break;
			case 'g': c[i]='c'; break;
			case 't': c[i]='a'; break;
			case 'A': c[i]='T'; break;
			case 'C': c[i]='G'; break;
			case 'G': c[i]='C'; break;
			case 'T': c[i]='A'; break;
			}
		}

		return new String(c);
	}	

	

			

}